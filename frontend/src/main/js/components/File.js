import React from "react"
import {Card, CardText, CardTitle, Progress} from "reactstrap"

class File extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const file = this.props.file;
        var cardColor = "info";

        if (file.priority < 15) {
            cardColor = "danger"
        } else if (file.priority < 30) {
            cardColor = "warning"
        } else if (file.priority < 40) {
            cardColor = "info"
        } else if (file.priority < 50) {
            cardColor = "secondary"
        } else if (file.priority < 60) {
            cardColor = "primary"
        } else if (file.priority >= 60) {
            cardColor = "success"
        }

        return (
            <Card body inverse color={cardColor}>
                <CardTitle>{file.file.name}</CardTitle>
                <CardText>
                    File size: [{file.offset} : {file.file.size}]<br/>
                    Last time modification: {file.file.lastTimeModification}<br/>
                    Client name: {file.client.name}<br/>
                    File priority: {file.priority}<br/>
                    <Progress value={file.offset / file.file.size * 100}/>
                </CardText>
            </Card>
        )
    }

}

export default File