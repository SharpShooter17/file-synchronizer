import React from "react"
import {Card, CardBody, CardFooter, CardHeader, Col, Row} from "reactstrap"
import Catalog from "./Catalog";

class ClientCatalog extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const clientCatalog = this.props.client;
        const client = clientCatalog.client;

        return (
            <Row>
                <Col>
                    <Card body outline color="secondary">
                        <CardHeader tag="h3">{client.name}
                            <span className="text-muted"> - {client.catalogPath}</span>
                        </CardHeader>
                        <CardBody>
                            <Catalog files={clientCatalog.files}/>
                        </CardBody>
                        <CardFooter className="text-muted">
                            {clientCatalog.ip}:{clientCatalog.port} - priority: {client.priority}
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
        )
    }

}

export default ClientCatalog;