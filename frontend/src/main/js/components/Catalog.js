import React from "react"
import {Table} from "reactstrap"

class Catalog extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const rows = this.props.files.map(file => (
            <tr>
                <td>{file.name}</td>
                <td>{file.size}</td>
                <td>{file.lastTimeModification}</td>
            </tr>
        ));

        return (
            <Table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Size</th>
                    <th>Last time modification</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </Table>
        )
    }

}

export default Catalog;