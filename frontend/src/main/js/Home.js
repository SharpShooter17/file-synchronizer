import React from 'react'
import {Client} from '@stomp/stompjs';
import {Button, CardColumns, Col, Row} from "reactstrap"
import ClientCatalog from "./components/ClientCatalog"
import api from "./api/Api";
import File from "./components/File";

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            clients: [],
            filesToSend: [],
            sendingFiles: [],
            receivingFiles: []
        };

        this.refreshClientCatalogs = this.refreshClientCatalogs.bind(this);
        this.refreshFilesToSend = this.refreshFilesToSend.bind(this);
        this.refreshSendingFiles = this.refreshSendingFiles.bind(this);
        this.refreshReceivingFiles = this.refreshReceivingFiles.bind(this);
    }

    componentDidMount() {
        this.refreshClientCatalogs();
        this.refreshFilesToSend();
        this.refreshSendingFiles();
        this.refreshReceivingFiles();

        this.client = new Client();
        this.client.configure({
            brokerURL: 'ws://localhost:' + window.location.port + '/stomp/websocket',
            onConnect: () => {
                this.client.subscribe('/topic/updates', message => {
                    if (message.body === 'client-catalog') {
                        this.refreshClientCatalogs();
                    } else if (message.body === 'files-to-send') {
                        this.refreshFilesToSend();
                    } else if (message.body === 'file-senders') {
                        this.refreshSendingFiles();
                    } else if (message.body === 'file-receivers') {
                        this.refreshReceivingFiles();
                    }
                });
            }
        });

        this.client.activate();

        setInterval(() => this.refreshSendingFiles(), 1000);
    }

    refreshClientCatalogs() {
        api.clientCatalog().done(response => {
            if (response.status.code < 300) {
                this.setState({clients: response.entity})
            }
        })
    }

    refreshFilesToSend() {
        api.filesToSend().done(response => {
            if (response.status.code < 300) {
                this.setState({filesToSend: response.entity})
            }
        })
    }

    refreshSendingFiles() {
        api.sendingFiles().done(response => {
            if (response.status.code < 300) {
                this.setState({sendingFiles: response.entity})
            }
        })
    }

    refreshReceivingFiles() {
        api.receivingFiles().done(response => {
            if (response.status.code < 300) {
                this.setState({receivingFiles: response.entity})
            }
        })
    }

    render() {

        const clients = this.state.clients.map(client => (<ClientCatalog client={client}/>));
        const filesToSend = this.state.filesToSend.map(file => (<File file={file}/>));
        const sendingFiles = this.state.sendingFiles.map(file => (<File file={file}/>));
        const receivingFiles = this.state.receivingFiles.map(file => (<File file={file}/>));

        return (
            <Row>
                <Col>
                    <Row>
                        <Col sm={11}>
                            <h1>Home page</h1>
                        </Col>
                        <Col sm={1}>
                            <Button className="btn btn-danger w-100" onClick={() => {
                                this.client.deactivate();
                                api.shutdown().done(result => {
                                    if (result.status.code < 300) {
                                        window.close();
                                    }
                                });
                            }}>
                                Shutdown
                            </Button>
                        </Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col>
                            <h5>Sending files</h5>
                            <CardColumns>
                                {sendingFiles}
                            </CardColumns>
                        </Col>
                        <Col>
                            <h5>Files to send</h5>
                            <CardColumns>
                                {filesToSend}
                            </CardColumns>
                        </Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col>
                            <h5>Receiving files</h5>
                            <CardColumns>
                                {receivingFiles}
                            </CardColumns>
                        </Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col>
                            <h5>Connected clients</h5>
                            {clients}
                        </Col>
                    </Row>
                </Col>
            </Row>
        )
    }
}

export default Home;