import ErrorHandleApi from "./ErrorHandleApi";

class Api extends ErrorHandleApi {

    constructor() {
        super('/api/');
    }

    name() {
        return this.get('name');
    }

    clientCatalog() {
        return this.get('client-catalogs');
    }

    filesToSend() {
        return this.get('files-to-send');
    }

    sendingFiles() {
        return this.get('file-senders');
    }

    receivingFiles() {
        return this.get('file-receivers');
    }

    shutdown() {
        return this.post('shutdown', null);
    }

}

const api = new Api();

export default api;