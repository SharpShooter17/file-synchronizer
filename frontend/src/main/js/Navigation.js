'use-strict';

import React from 'react';
import {Navbar, NavbarBrand} from 'reactstrap';
import api from './api/Api'

class Navigation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }
    }

    componentDidMount() {
        api.name().done(response => {
            if (response.status.code < 300) {
                this.setState({name: response.entity});
            }
        })
    }

    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">File synchronizer: {this.state.name}</NavbarBrand>
                </Navbar>
            </div>
        );
    }
}

export default Navigation;