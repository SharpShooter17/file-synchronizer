'use strict';

import React from "react";
import ReactDOM from "react-dom";
import {HashRouter, Route, Switch} from "react-router-dom";
import {ToastContainer} from "react-toastify"
import {Col, Container, Row} from "reactstrap";

import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Navigation from "./Navigation"
import Home from "./Home";

class App extends React.Component {
    render() {
        return (
            <div>
                <HashRouter hashType="slash">
                    <div>
                        <Navigation/>
                        <Container fluid={true}>
                            <Row>
                                <Col>
                                    <Switch>
                                        <Route exac path="/" component={Home}/>
                                    </Switch>
                                </Col>
                            </Row>
                            <ToastContainer position="top-right"
                                            autoClose={10000}
                                            hideProgressBar={false}
                                            newestOnTop={false}
                                            closeOnClick={false}
                                            rtl={false}
                                            pauseOnFocusLoss={false}
                                            draggable={true}
                                            pauseOnHover={false}/>
                        </Container>
                    </div>
                </HashRouter>
            </div>
        )
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('react')
);