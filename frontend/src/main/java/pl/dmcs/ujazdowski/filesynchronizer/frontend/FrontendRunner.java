package pl.dmcs.ujazdowski.filesynchronizer.frontend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import pl.dmcs.ujazdowski.filesynchronizer.core.api.FileSynchronizerApi;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners.ConnectionListener;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners.FileReceiversListener;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners.FileSendersListener;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners.FilesToSendListener;

@SpringBootApplication
public class FrontendRunner extends SpringBootServletInitializer {

    private static FileSynchronizerApi<? extends ConnectionI> fileSynchronizerApi;

    @Bean
    public FileSynchronizerApi<? extends ConnectionI> api(@Autowired ConnectionListener connectionListener,
                                                @Autowired FilesToSendListener filesToSendListener,
                                                @Autowired FileSendersListener fileSendersListener,
                                                @Autowired FileReceiversListener fileReceiversListener) {
        fileSynchronizerApi.addConnectionListener(connectionListener);
        fileSynchronizerApi.addFilesToSendListener(filesToSendListener);
        fileSynchronizerApi.addFileSendersListener(fileSendersListener);
        fileSynchronizerApi.addReceiversListener(fileReceiversListener);
        return FrontendRunner.fileSynchronizerApi;
    }

    public static ConfigurableApplicationContext start(FileSynchronizerApi<? extends ConnectionI> fileSynchronizerApi, String[] args) {
        FrontendRunner.fileSynchronizerApi = fileSynchronizerApi;
        return SpringApplication.run(FrontendRunner.class, args);
    }

}
