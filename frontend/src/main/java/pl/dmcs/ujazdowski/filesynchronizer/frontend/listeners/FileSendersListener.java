package pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners;

import javafx.collections.ListChangeListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileSender;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.ws.StompController;

@Component
public class FileSendersListener implements ListChangeListener<FileSender> {

    private final StompController stompController;

    @Autowired
    public FileSendersListener(StompController stompController) {
        this.stompController = stompController;
    }

    @Override
    public void onChanged(Change<? extends FileSender> c) {
        stompController.resourceUpdated("file-senders");
    }

}
