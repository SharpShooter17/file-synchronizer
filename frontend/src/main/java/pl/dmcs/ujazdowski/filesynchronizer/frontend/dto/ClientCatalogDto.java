package pl.dmcs.ujazdowski.filesynchronizer.frontend.dto;

import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher.CatalogWatchConnection;

import java.io.Serializable;
import java.util.Collection;

public class ClientCatalogDto implements Serializable {

    private static final long serialVersionUID = 8870244181260284658L;

    private final Client client;

    private final String ip;

    private final int port;

    private final Collection<FileDescription> files;

    public ClientCatalogDto(CatalogWatchConnection catalogWatchConnection) {
        this.client = catalogWatchConnection.connection().client();
        this.ip = catalogWatchConnection.connection().socket().getInetAddress().toString();
        this.port = catalogWatchConnection.connection().socket().getPort();
        this.files = catalogWatchConnection.files();
    }

    public Client getClient() {
        return client;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public Collection<FileDescription> getFiles() {
        return files;
    }

}
