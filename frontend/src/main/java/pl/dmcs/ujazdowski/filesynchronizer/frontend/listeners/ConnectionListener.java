package pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners;

import javafx.collections.ListChangeListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher.CatalogWatchConnection;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.ws.StompController;

@Component
public class ConnectionListener implements ListChangeListener<CatalogWatchConnection> {

    private final StompController stompController;

    @Autowired
    public ConnectionListener(StompController stompController) {
        this.stompController = stompController;
    }

    @Override
    public void onChanged(Change<? extends CatalogWatchConnection> c) {
        stompController.resourceUpdated("client-catalog");
    }

}
