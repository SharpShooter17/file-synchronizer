package pl.dmcs.ujazdowski.filesynchronizer.frontend.ws;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class StompController {

    private final SimpMessagingTemplate template;

    public StompController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping("/updates")
    @SendTo("/topic/updates")
    public String tasks(String resource) {
        return resource;
    }

    public void resourceUpdated(String resource) {
        this.template.convertAndSend("/topic/updates", resource);
    }

}
