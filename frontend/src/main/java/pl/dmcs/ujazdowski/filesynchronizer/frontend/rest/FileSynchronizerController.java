package pl.dmcs.ujazdowski.filesynchronizer.frontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.dmcs.ujazdowski.filesynchronizer.core.api.FileSynchronizerApi;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.dto.ClientCatalogDto;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.dto.FileDto;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/")
public class FileSynchronizerController {

    private final FileSynchronizerApi<? extends ConnectionI> api;

    @Autowired
    public FileSynchronizerController(FileSynchronizerApi<? extends ConnectionI> api) {
        this.api = api;
    }

    @GetMapping(value = "name", produces = "application/json")
    public String name() {
        return api.getName();
    }

    @GetMapping(value = "client-catalogs", produces = "application/json")
    public Collection<ClientCatalogDto> catalogs() {
        return api.getConnections().read(list -> list.stream().map(ClientCatalogDto::new).collect(Collectors.toList()));
    }

    @GetMapping(value = "files-to-send", produces = "application/json")
    public Collection<FileDto> filesToSend() {
        return api.filesToSend().read(list -> list.stream().map(FileDto::new).collect(Collectors.toList()));
    }

    @GetMapping(value = "file-senders", produces = "application/json")
    public Collection<FileDto> fileSenders() {
        return api.sendingFiles().stream().map(FileDto::new).collect(Collectors.toList());
    }

    @GetMapping(value = "file-receivers", produces = "application/json")
    public Collection<FileDto> fileReceivers() {
        return api.fileReceivers().read(list -> list.stream().map(FileDto::new).collect(Collectors.toList()));
    }

    @PostMapping(value = "shutdown")
    public void shutdown() {
        api.shutdown();
    }

}
