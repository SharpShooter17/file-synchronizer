package pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners;

import javafx.collections.ListChangeListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.receiver.FileReceiver;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileToSend;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.ws.StompController;

@Component
public class FileReceiversListener implements ListChangeListener<FileReceiver> {

    private final StompController stompController;

    @Autowired
    public FileReceiversListener(StompController stompController) {
        this.stompController = stompController;
    }

    @Override
    public void onChanged(Change<? extends FileReceiver> c) {
        stompController.resourceUpdated("file-receivers");
    }

}
