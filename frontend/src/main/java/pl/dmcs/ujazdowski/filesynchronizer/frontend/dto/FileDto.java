package pl.dmcs.ujazdowski.filesynchronizer.frontend.dto;

import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileMapper;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.receiver.FileReceiver;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileToSend;

import java.math.BigDecimal;
import java.time.Instant;

public class FileDto {

    private final Long offset;

    private final FileDescription file;

    private final Client client;

    private final BigDecimal priority;

    public FileDto(FileToSend fileToSend) {
        this.offset = fileToSend.offset();
        this.file = FileMapper.map(fileToSend.file());
        this.client = fileToSend.connection().client();
        this.priority = fileToSend.priority();
    }

    public FileDto(FileReceiver receiver) {
        this.offset = 0L;
        this.client = receiver.connection().client();
        this.priority = BigDecimal.valueOf(receiver.connection().client().getPriority().value);
        this.file = new FileDescription(receiver.filename(), 0L, Instant.now());
    }

    public Long getOffset() {
        return offset;
    }

    public FileDescription getFile() {
        return file;
    }

    public Client getClient() {
        return client;
    }

    public BigDecimal getPriority() {
        return priority;
    }

}
