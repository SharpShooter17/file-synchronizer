package pl.dmcs.ujazdowski.filesynchronizer.frontend.listeners;

import javafx.collections.ListChangeListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileToSend;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.ws.StompController;

@Component
public class FilesToSendListener implements ListChangeListener<FileToSend> {

    private final StompController stompController;

    @Autowired
    public FilesToSendListener(StompController stompController) {
        this.stompController = stompController;
    }

    @Override
    public void onChanged(Change<? extends FileToSend> c) {
        stompController.resourceUpdated("files-to-send");
    }

}
