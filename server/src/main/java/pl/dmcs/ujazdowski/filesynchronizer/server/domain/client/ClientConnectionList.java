package pl.dmcs.ujazdowski.filesynchronizer.server.domain.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.Message;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Consumer;

public class ClientConnectionList implements ConnectionI {

    private static final Logger log = LoggerFactory.getLogger(ClientConnectionList.class);

    private final Set<ClientConnection> connections = new ConcurrentSkipListSet<>();

    public void add(ClientConnection client) {
        log.info("Add new client {}", client);
        connections.add(client);
    }

    public void remove(ClientConnection client) {
        connections.remove(client);
    }

    @Override
    public <T extends ConnectionI> void invokeIfHasMessage(Consumer<T> consumer) {
        connections.forEach(clientConnection -> clientConnection.invokeIfHasMessage(consumer));
    }

    @Override
    public Message readMessage() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendMessage(Message message) {
        connections.forEach(connection -> connection.sendMessage(message));
    }

}
