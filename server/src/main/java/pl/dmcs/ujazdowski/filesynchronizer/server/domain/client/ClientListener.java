package pl.dmcs.ujazdowski.filesynchronizer.server.domain.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.exception.ApplicationException;

import java.net.ServerSocket;
import java.net.Socket;

public class ClientListener extends Thread {

    private static Logger log = LoggerFactory.getLogger(ClientListener.class);

    private final ServerSocket serverSocket;

    private final ServerService clientService;

    public ClientListener(ServerSocket serverSocket, ServerService clientService) {
        super(ClientListener.class.getSimpleName());
        this.serverSocket = serverSocket;
        this.clientService = clientService;
    }

    @Override
    public void run() {
        log.info("Client listener start");

        while (!Thread.currentThread().isInterrupted()) {
            try {
                Socket clientSocket = serverSocket.accept();
                clientService.addClient(clientSocket);
            } catch (Exception e) {
                throw new ApplicationException("Client listener fail", e);
            }
        }

        log.info("Client listener stop");
    }

}
