package pl.dmcs.ujazdowski.filesynchronizer.server;

import org.springframework.context.ConfigurableApplicationContext;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.FrontendRunner;

import java.io.IOException;

public class ServerApp {

    public static void main(String[] args) throws InterruptedException, IOException {
        FileSynchronizerServer runner = FileSynchronizerServer.initialize();
        ConfigurableApplicationContext frontendContext = FrontendRunner.start(runner.api(), args);
        runner.join();
        frontendContext.close();
    }

}
