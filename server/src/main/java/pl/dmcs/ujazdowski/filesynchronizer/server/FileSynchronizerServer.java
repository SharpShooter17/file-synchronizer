package pl.dmcs.ujazdowski.filesynchronizer.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.config.Configuration;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.handler.*;
import pl.dmcs.ujazdowski.filesynchronizer.core.runner.FileSynchronizerRunner;
import pl.dmcs.ujazdowski.filesynchronizer.server.domain.Server;
import pl.dmcs.ujazdowski.filesynchronizer.server.domain.client.ClientConnection;
import pl.dmcs.ujazdowski.filesynchronizer.server.domain.client.ClientListener;
import pl.dmcs.ujazdowski.filesynchronizer.server.domain.client.ServerService;
import pl.dmcs.ujazdowski.filesynchronizer.server.domain.message.handler.RegistrationMessageHandler;

import java.io.IOException;
import java.net.ServerSocket;

import static pl.dmcs.ujazdowski.filesynchronizer.server.ServerConfig.USER_CATALOGS;

public class FileSynchronizerServer extends FileSynchronizerRunner<ClientConnection, ServerService> {
    private static Logger log = LoggerFactory.getLogger(FileSynchronizerServer.class);

    private final Server server;

    private final ClientListener clientListener;

    public FileSynchronizerServer(Configuration<ClientConnection, ServerService> configuration, Server server, ClientListener clientListener) throws IOException {
        super(configuration);
        this.server = server;
        this.clientListener = clientListener;
    }

    public static FileSynchronizerServer initialize() throws IOException {
        log.info("File synchronizer server is starting...");

        Server server = new Server();
        ServerSocket serverSocket = server.start(ServerConfig.PORT);

        ServerService service = new ServerService();

        ClientListener clientListener = new ClientListener(serverSocket, service);

        MessageHandler firstMessageHandler = new FilePartMessageHandler(service);
        firstMessageHandler
                .next(new FileDeleteMessageHandler(service))
                .next(new FileListResponseHandler(service))
                .next(new FileDownloadMessageHandler(service))
                .next(new FileListMessageHandler(service))
                .next(new RegistrationMessageHandler(service))
                .next(new UnregisterMessageHandler(service))
                .next(new UnhandledMessageHandler());

        FileSynchronizerServer runner = new FileSynchronizerServer(
                new Configuration(
                        ServerConfig.MAX_FILE_SENDER_THREADS,
                        service.list(),
                        firstMessageHandler,
                        service
                ),
                server,
                clientListener);

        runner.initializeApi();
        runner.start();
        return runner;
    }

    @Override
    public void start() {
        super.start();
        this.clientListener.start();
    }

    @Override
    public void stop() {
        server.stop();
        clientListener.interrupt();
        super.stop();
        log.info("File synchronizer server STOP");
    }

    @Override
    public void join() throws InterruptedException {
        super.join();
        clientListener.join();
    }

    @Override
    public void stopThreads() {
        super.stopThreads();
        clientListener.interrupt();
    }

    private void initializeApi() {
        api().setName("Server: " + USER_CATALOGS);
    }

}
