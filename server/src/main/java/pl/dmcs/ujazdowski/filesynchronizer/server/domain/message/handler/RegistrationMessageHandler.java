package pl.dmcs.ujazdowski.filesynchronizer.server.domain.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileListMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.RegisterMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.handler.MessageHandler;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;
import pl.dmcs.ujazdowski.filesynchronizer.server.ServerConfig;
import pl.dmcs.ujazdowski.filesynchronizer.server.domain.client.ClientConnection;

public class RegistrationMessageHandler extends MessageHandler<RegisterMessage, ClientConnection> {

    private final Service service;

    public RegistrationMessageHandler(Service service) {
        super(MessageType.REGISTER);
        this.service = service;
    }

    @Override
    protected void handle(RegisterMessage message) {
        Client client = message.client();
        client.setCatalogPath(ServerConfig.USER_CATALOGS + "\\" + client.getName());
        service.register(message.sender(), client);

        message.sender().sendMessage(new FileListMessage());
    }

}
