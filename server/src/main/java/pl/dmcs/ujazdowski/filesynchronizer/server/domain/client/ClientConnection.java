package pl.dmcs.ujazdowski.filesynchronizer.server.domain.client;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;

import java.io.IOException;
import java.net.Socket;
import java.time.Instant;
import java.util.Objects;

public class ClientConnection extends AbstractConnection implements Comparable<ClientConnection> {

    private final Instant connectedAt;

    public ClientConnection(Socket socket) throws IOException {
        super(socket);
        this.connectedAt = Instant.now();
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        ClientConnection that = (ClientConnection) o;
        return Objects.equals(client, that.client);
    }

    @Override
    public int hashCode() {
        return Objects.hash(client);
    }

    @Override
    public String toString() {
        if ( isRegistered() ) {
            return this.client.toString();
        } else {
            return this.socket.getInetAddress() + ":" + this.socket.getPort();
        }
    }

    @Override
    public int compareTo(ClientConnection o) {
        return this.connectedAt.compareTo(o.connectedAt);
    }

}
