package pl.dmcs.ujazdowski.filesynchronizer.server.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.exception.ApplicationException;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {
    private static Logger log = LoggerFactory.getLogger(Server.class);

    private ServerSocket socket;

    public ServerSocket start(int port) {
        try {
            socket = new ServerSocket(port);
        } catch (IOException e) {
            throw new ApplicationException("Cannot start server", e);
        }
        log.info("Server started: {}", socket.isBound());

        return socket;
    }

    public void stop() {
        try {
            socket.close();
        } catch (IOException e) {
            throw new ApplicationException("Cannot stop server", e);
        }
        log.info("Server stopped");
    }

}
