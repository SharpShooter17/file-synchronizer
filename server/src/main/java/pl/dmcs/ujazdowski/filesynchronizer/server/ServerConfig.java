package pl.dmcs.ujazdowski.filesynchronizer.server;

public class ServerConfig {

    public static final int PORT = Integer.parseInt(System.getenv("filesynchronizer.server-port"));

    public static final String USER_CATALOGS = System.getenv("filesynchronizer.server.user.catalog");

    public static final int MAX_FILE_SENDER_THREADS = 3;

    private ServerConfig() {
    }

}
