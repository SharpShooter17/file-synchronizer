package pl.dmcs.ujazdowski.filesynchronizer.server.domain.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

import java.io.IOException;
import java.net.Socket;

public class ServerService extends Service {

    private static final Logger log = LoggerFactory.getLogger(ServerService.class);

    private final ClientConnectionList connectionPool = new ClientConnectionList();

    public void addClient(Socket clientSocket) throws IOException {
        try {
            ClientConnection client = new ClientConnection(clientSocket);
            connectionPool.add(client);
        } catch (IOException e) {
            log.error("Cannot add client to list!", e);
            clientSocket.close();
        }
    }

    public ClientConnectionList list() {
        return connectionPool;
    }

    @Override
    public void unregister(AbstractConnection connection) {
        connectionPool.remove((ClientConnection) connection);
        super.unregister(connection);
    }

}
