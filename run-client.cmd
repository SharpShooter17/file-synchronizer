SET client.name=%1
SET client.path=%2\%1
SET client.priority=%3
SET filesynchronizer.server.ip=%4
SET filesynchronizer.server-port=%5
SET file.sender.thread.max=%6
SET server.port=%7

call java -jar ./client/target/filesynchronizer-client.jar