Należy opracować aplikację kliencką i serwerową przy podanych poniżej założeniach:
Aplikacja kliencka:
- [x] Operuje na dwóch głównych parametrach: nazwa użytkownika i ścieżka do lokalnego folderu.
- [x] Klient po uruchomieniu rejestruje się na serwerze (podając swoja nazwę użytkownika).
- [x] Każdy klient ma swój lokalny folder z plikami.
- [x] Aplikacja obserwuje lokalny folder i reaguje na zmiany. Jak pojawią się tam nowe pliki, to wysyła je na serwer.
- [x] Jak pojawi się nowy plik dla danego użytkownika, to jest on pobierany do lokalnego folderu.
- [x] Aplikacja po uruchomieniu odpytuje serwer o nowe pliki i je ściąga.
- [x] Aplikacja kliencka ma interfejs graficzny (np. Java FX) pokazujący w czasie rzeczywistym czym się w danej chwili zajmuje klient ("Pobieram...", "Wysyłam ...", "Sprawdzam ....") oraz wyświetlający listę aktualnych plików w lokalnym folderze. 
- [x] Klient po zamknięciu informuje serwer że się zamyka i serwer usuwa go z listy użytkowników dostępnych.
Serwer:
- [x] Każdy użytkownik ma swój katalog na serwerze.
- [x] Serwer posiada panel graficzny pokazujący czym się w danej chwili zajmuje i jaką kolejką zadań dysponuje, jakie operacje na niej wykonuje.
- [x] Serwer posiada panel graficzny pokazujący zawartość katalogów.
- [x] Serwer dynamicznie odświeża liczbę klientów.

Uwagi:
- [x] Wysyłanie / odbieranie plików przez klienta dzieje się przy wykorzystaniu puli wątków (co najmniej jeden watek dla każdego pliku).
- [x] Aplikacja klienta monitoruje zawartość katalogu lokalnego i po usunięciu pliku w sposób "ręczny" z katalogu lokalnego odświeża stan i w kliencie i na serwerze.
- [x] Należy rozważyć jeden wątek sterujący po stronie serwera tzw. load balancer lub scheduler.
- [x] Należy przygotować plik lub klasę zawierające konfigurację serwera i klientów z np. ilością wątków, priorytetami.
- [x] Należy rozważyć sytuację, w której jeden z klientów wysyła plik dużych rozmiarów.
- [x] Należy rozważyć sytuację, w której jeden z klientów wysyła wiele małych plików.
- [x] Należy rozważyć sytuację, w której jeden z klientów pojawia się w trakcie dłuższego działania i obciążenia systemu.
- [x] Aby nie generować plików w lokalnym systemie możliwe jest wprowadzenie opóźnienia w wysyłanych danych i symulacja jakości transferu.
- [x] Implementacja możliwa w dowolnym języku, możliwe jest wykorzystanie zewnętrznych bibliotek, jednak podstawa przedmiotu, czyli logika współbieżna, powinna być napisana samodzielnie od podstaw.
- [ ] Sugerowane jest opracowanie i prezentacja algorytmu przed rozpoczęciem prac programistycznych.


Czy serwer powinien w sposób obsługiwać:
-anulowanie pobierania przez klienta
-utrata połączenia z klientem w trakcie pobierania/wysyłania
Czy pomijamy taki błąd i zabraniamy rozłączyć się klientowi w trakcie przesyłu danych?
Generalnie w relacji klient-serwer nie mozemy zabronic klientowi rozlaczyc sie, ale prosze zaimplementowac najprostsza wersje obslugi, 
czyli ponowne pobieranie od poczatku przy kolejnym podlaczeniu klienta

1. Pamiętajcie proszę, że pracujemy z problemami współbieżnymi i chciałbym, aby Wasze programy i polityki obsługi klientów, które wprowadzacie, generowały takie sytuacje.
Dla jasności: gdy mamy kolejkę plików i obsługujemy je jeden po drugim, bądź co dwa, trzy to mamy programowanie wielowątkowe. Gdyby te wątki czytały z jednej kolejki, 
to ta kolejka jest procedowana równolegle. Współbieżność natomiast pojawia się gdy wątki stają się od siebie zależne. Czyli np synchronizujemy duże pliki, pojawia się mały 
i aby nie czekał, aż skończymy synchronizować duże, zatrzymujemy w wątku synchronizacje pliku dużego i rozpoczynamy mały - tu mamy wspolbieznosc tego wątku. 
Odnoszac się do przykładu z wykładu "jedzący filozofowie": jeżeli mamy 5 filozofów i 10 widelcy to jedzą równolegle, kazdy ma swoje zasoby, nic z zewnątrz nikogo nie interesuje, jeżeli zabierzemy choć jeden widelec i jest ich 9, to już muszą się skomunikować, są od siebie zależni i tu mamy podejście współbieżne.
2. Często w trakcie rozmów doradzam, że łatwo zrealizować taką sytuację gdy wprowadzony zostanie podwójny limit po stronie serwera. Np 5 wątków działających równolegle i równocześnie i 8 wątków dostępnych ogólnie. Oznacza to, że trzy z nich mogą pracować współbieżnie. 
Np: wątki 1-5 synchronizują pliki, pojawia się nowy do obsłużenia od razu, wątek5 ma pauze, rusza wątek6.