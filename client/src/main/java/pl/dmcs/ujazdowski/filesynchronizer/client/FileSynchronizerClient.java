package pl.dmcs.ujazdowski.filesynchronizer.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.client.domain.ClientService;
import pl.dmcs.ujazdowski.filesynchronizer.client.domain.ServerConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.config.Configuration;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.handler.*;
import pl.dmcs.ujazdowski.filesynchronizer.core.runner.FileSynchronizerRunner;

import java.io.IOException;

public class FileSynchronizerClient extends FileSynchronizerRunner<ServerConnection, ClientService> {
    private static Logger log = LoggerFactory.getLogger(FileSynchronizerClient.class);

    public FileSynchronizerClient(Configuration<ServerConnection, ClientService> configuration) throws IOException {
        super(configuration);
    }

    public static FileSynchronizerClient initialize() throws IOException {
        log.info("File synchronizer client is starting...");

        ServerConnection connection = ServerConnection.connect();
        ClientService service = new ClientService(connection);

        MessageHandler firstMessageHandler = new FilePartMessageHandler(service);
        firstMessageHandler
                .next(new FileDeleteMessageHandler(service))
                .next(new FileDownloadMessageHandler(service))
                .next(new FileListMessageHandler(service))
                .next(new FileListResponseHandler(service))
                .next(new UnregisterMessageHandler(service))
                .next(new UnhandledMessageHandler());


        FileSynchronizerClient runner = new FileSynchronizerClient(
                new Configuration<>(
                        Integer.parseInt(System.getenv("file.sender.thread.max")),
                        connection,
                        firstMessageHandler,
                        service
                ));

        service.setRunner(runner);

        service.register();
        service.fileList();

        runner.initializeApi();
        runner.start();
        log.info("File synchronizer client is running");
        return runner;
    }

    @Override
    public void stop() {
        super.stop();
        connection.disconnect();
        log.info("File synchronizer client STOP");
    }

    void initializeApi() {
        api().setName(connection.client().getName() + ": " + connection.client().getCatalogPath());
    }

}
