package pl.dmcs.ujazdowski.filesynchronizer.client.configuration;

import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;
import pl.dmcs.ujazdowski.filesynchronizer.core.client.Priority;

public class ConfigurationHolder {

    private static ConfigurationHolder instance = null;

    public final Client client;

    public final ServerAddress serverAddress;

    private ConfigurationHolder(Client client,
                                ServerAddress serverAddress) {
        this.client = client;
        this.serverAddress = serverAddress;
    }

    private static void initialize() {
        instance = new ConfigurationHolder(
                new Client(
                        System.getenv("client.name"),
                        System.getenv("client.path"),
                        Priority.valueOf(System.getenv("client.priority"))),
                new ServerAddress(
                        System.getenv("filesynchronizer.server.ip"),
                        Integer.parseInt(System.getenv("filesynchronizer.server-port")))
        );
    }

    public static ConfigurationHolder instance() {
        if ( instance == null ) initialize();
        return instance;
    }

}
