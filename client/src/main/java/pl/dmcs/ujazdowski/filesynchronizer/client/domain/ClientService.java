package pl.dmcs.ujazdowski.filesynchronizer.client.domain;

import pl.dmcs.ujazdowski.filesynchronizer.client.configuration.ConfigurationHolder;
import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileListMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.RegisterMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.runner.FileSynchronizerRunner;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

public class ClientService extends Service {

    private final ServerConnection connection;

    private FileSynchronizerRunner<ServerConnection, ClientService> runner;

    public ClientService(ServerConnection serverConnection) {
        super();
        connection = serverConnection;
    }

    public void register() {
        Client client = ConfigurationHolder.instance().client;
        connection.sendMessage(new RegisterMessage(client));
        super.register(connection, client);
    }

    public void fileList() {
        connection.sendMessage(new FileListMessage());
    }

    @Override
    public void unregister(AbstractConnection connection) {
        runner.stop();
    }

    public void setRunner(FileSynchronizerRunner<ServerConnection, ClientService> runner) {
        this.runner = runner;
    }

}
