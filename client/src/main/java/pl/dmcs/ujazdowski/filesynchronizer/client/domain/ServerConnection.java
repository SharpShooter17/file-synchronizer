package pl.dmcs.ujazdowski.filesynchronizer.client.domain;

import pl.dmcs.ujazdowski.filesynchronizer.client.configuration.ConfigurationHolder;
import pl.dmcs.ujazdowski.filesynchronizer.client.configuration.ServerAddress;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.exception.ApplicationException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class ServerConnection extends AbstractConnection {

    private ServerConnection(Socket socket) throws IOException {
        super(socket);
    }

    public static ServerConnection connect() {
        try {
            ServerAddress serverAddress = ConfigurationHolder.instance().serverAddress;
            Socket socket = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(serverAddress.ip, serverAddress.port);
            socket.connect(socketAddress, 5000);
            return new ServerConnection(socket);
        } catch (IOException e) {
            throw new ApplicationException("Cannot connect to server", e);
        }
    }

}