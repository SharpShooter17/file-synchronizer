package pl.dmcs.ujazdowski.filesynchronizer.client.configuration;

public class ServerAddress {

    public final String ip;

    public final int port;

    public ServerAddress(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

}
