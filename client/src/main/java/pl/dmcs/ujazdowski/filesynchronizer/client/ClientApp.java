package pl.dmcs.ujazdowski.filesynchronizer.client;

import org.springframework.context.ConfigurableApplicationContext;
import pl.dmcs.ujazdowski.filesynchronizer.frontend.FrontendRunner;

import java.io.IOException;

public class ClientApp {

    public static void main(String[] args) throws IOException, InterruptedException {
        FileSynchronizerClient client = FileSynchronizerClient.initialize();
        ConfigurableApplicationContext frontendContext = FrontendRunner.start(client.api(), args);
        client.join();
        frontendContext.close();
    }

}
