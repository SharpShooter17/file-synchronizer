package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.UnregisterMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

public class UnregisterMessageHandler extends MessageHandler<UnregisterMessage, AbstractConnection> {

    private final Service service;

    public UnregisterMessageHandler(Service clientService) {
        super(MessageType.UNREGISTER);
        this.service = clientService;
    }

    @Override
    protected void handle(UnregisterMessage message) {
        service.unregister(message.sender());
    }

}
