package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.UnhandledMessage;

public class UnhandledMessageHandler extends MessageHandler<UnhandledMessage, AbstractConnection> {

    public UnhandledMessageHandler() {
        super(MessageType.UNHANDLED);
    }

    @Override
    protected void handle(UnhandledMessage message) {
        log.error("Unhandled message {}", message);
    }

}
