package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileDownloadMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileListResponse;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

import java.util.Collection;
import java.util.stream.Collectors;

public class FileListResponseHandler extends MessageHandler<FileListResponse, AbstractConnection> {

    private final Service service;

    public FileListResponseHandler(Service service) {
        super(MessageType.FILE_LIST_RESPONSE);
        this.service = service;
    }

    @Override
    protected void handle(FileListResponse message) {
        log.info("Files on second side:\n{}", this.listFiles(message.files()));

        Collection<FileDownloadMessage> filesToUpdate = service.filesToUpdate(message.sender().client(), message.files());
        filesToUpdate.forEach(file -> message.sender().sendMessage(file));

        log.info("Files to update:\n{}", listFiles(filesToUpdate));
    }

    private String listFiles(Collection<?> files) {
        return files.stream().map(Object::toString)
                .collect(Collectors.joining("\n"));
    }

}