package pl.dmcs.ujazdowski.filesynchronizer.core.file.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.exception.ApplicationException;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher.CatalogWatcher;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FilePartMessage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static pl.dmcs.ujazdowski.filesynchronizer.core.file.FileConfiguration.FILE_PART_SUFFIX;
import static pl.dmcs.ujazdowski.filesynchronizer.core.file.FileConfiguration.FILE_RECEIVING_TIMEOUT_MS;

public class FileReceiver extends Thread {

    private final Logger log;

    private final String filename;

    private final Queue<FilePartMessage> fileParts = new ConcurrentLinkedQueue<>();

    private final CatalogWatcher catalogWatcher;

    private final AbstractConnection connection;

    public FileReceiver(String filename,
                        CatalogWatcher catalogWatcher,
                        AbstractConnection connection) {
        super(FileReceiver.class.getSimpleName() + " " + filename);
        this.log = LoggerFactory.getLogger("FileReceiver: " + filename);
        this.filename = filename;
        this.catalogWatcher = catalogWatcher;
        this.connection = connection;
    }

    @Override
    public void run() {
        log.info("Start receiving file");

        File file = prepareFile();

        boolean runPostProcessing;
        try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file, true))) {
            runPostProcessing = processFile(outputStream, file);
        } catch (Exception e) {
            throw new ApplicationException("Cannot open stream", e);
        }

        if ( runPostProcessing ) {
            postProcess(file);
        }

        log.info("Stop receiving file");
    }

    private boolean processFile(OutputStream outputStream, File file) {
        long lastFilePart = System.currentTimeMillis();
        while (!Thread.currentThread().isInterrupted()) {
            if ( timeout(lastFilePart) ) {
                log.info("File receiver timeout");
                return false;
            } else if ( fileParts.isEmpty() ) {
                yield();
            } else {
                FilePartMessage message = fileParts.poll();
                saveFilePart(message, outputStream);
                if ( message.size() == file.length() ) {
                    if ( !file.setLastModified(message.modificationTimestamp()) ) {
                        log.error("Cannot change last modified date");
                    }
                    return true;
                }
                lastFilePart = System.currentTimeMillis();
            }
        }

        return false;
    }

    private boolean timeout(long lastFilePart) {
        return System.currentTimeMillis() - lastFilePart > FILE_RECEIVING_TIMEOUT_MS;
    }

    private void postProcess(File file) {
        Path targetPath = file.toPath().resolveSibling(filename);
        catalogWatcher.receivedNewFile(
                new FileDescription(
                        targetPath.getFileName().toString(),
                        file.length(),
                        Instant.ofEpochMilli(file.lastModified())
                ),
                connection
        );

        try {
            Files.move(file.toPath(), targetPath, REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("Cannot rename file: {} to {}", file.toPath(), targetPath);
        }
    }

    private File prepareFile() {
        File file = new File(filename + FILE_PART_SUFFIX);

        if ( !file.exists() ) {
            try {
                if ( !file.createNewFile() ) {
                    throw new ApplicationException("Cannot create file: " + file.getPath());
                }
            } catch (IOException e) {
                throw new ApplicationException(e);
            }
        }

        return file;
    }

    private void saveFilePart(FilePartMessage message, OutputStream outputStream) {
        try {
            outputStream.write(message.filePart());
            outputStream.flush();
        } catch (IOException e) {
            log.error("Cannot write to file");
        }
    }

    public String filename() {
        return filename;
    }

    public final AbstractConnection connection() {
        return this.connection;
    }

    synchronized void addFilePart(FilePartMessage filePartMessage) {
        if ( !filePartMessage.filename().equals(filename()) ) {
            throw new ApplicationException("File mismatched!");
        }
        fileParts.offer(filePartMessage);
    }

}
