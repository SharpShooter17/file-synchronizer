package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FilePartMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

import java.nio.file.Paths;

public class FilePartMessageHandler extends MessageHandler<FilePartMessage, AbstractConnection> {

    private final Service service;

    public FilePartMessageHandler(Service service) {
        super(MessageType.FILE_PART);
        this.service = service;
    }

    @Override
    protected void handle(FilePartMessage message) {
        log.debug("File part message: {}", message.filename());
        message.filename(Paths.get(message.sender().client().getCatalogPath(), message.filename()).toString());
        service.saveFile(message);
    }

}
