package pl.dmcs.ujazdowski.filesynchronizer.core.file.receiver;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.concurrent.Shared;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher.CatalogWatcher;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FilePartMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.stream.Collectors;

public class FileReceiverSupervisor extends Thread {

    private static final Logger log = LoggerFactory.getLogger(FileReceiverSupervisor.class);

    private final Shared<ObservableList<FileReceiver>> receivers = new Shared<>(new ObservableListWrapper<>(new ArrayList<>()));

    private final Queue<FilePartMessage> fileQueue = new ConcurrentLinkedDeque<>();

    private final CatalogWatcher catalogWatcher;

    public FileReceiverSupervisor(CatalogWatcher catalogWatcher) {
        super(FileReceiverSupervisor.class.getSimpleName());
        this.catalogWatcher = catalogWatcher;
    }

    @Override
    public void run() {
        log.info("Start File Receiver Supervisor");

        while (!Thread.currentThread().isInterrupted()) {
            clearInactiveReceivers();

            if ( fileQueue.isEmpty() ) {
                yield();
            } else {
                sendFilePartToReceiver();
            }
        }

        this.receivers.write(list -> list.forEach(Thread::interrupt));

        log.info("Stop File Receiver Supervisor");
    }

    private void sendFilePartToReceiver() {
        FilePartMessage filePartMessage = fileQueue.poll();
        FileReceiver fileReceiver = receivers.read(list -> list.stream()
                .filter(receiver -> receiver.filename().equals(filePartMessage.filename()))
                .findFirst()
                .orElse(new FileReceiver(filePartMessage.filename(), catalogWatcher, filePartMessage.sender())));

        if ( !fileReceiver.isAlive() ) {
            receivers.write(list -> list.add(fileReceiver));
            fileReceiver.start();
        }

        fileReceiver.addFilePart(filePartMessage);
    }

    private void clearInactiveReceivers() {
        List<FileReceiver> notAliveReceivers = receivers.read(list -> list.stream()
                .filter(receiver -> !receiver.isAlive())
                .collect(Collectors.toList()));
        receivers.write(list -> list.removeAll(notAliveReceivers));
    }

    public void addFilePart(FilePartMessage filePartMessage) {
        fileQueue.add(filePartMessage);
    }

    public final Shared<ObservableList<FileReceiver>> runningReceivers() {
        return this.receivers;
    }

}