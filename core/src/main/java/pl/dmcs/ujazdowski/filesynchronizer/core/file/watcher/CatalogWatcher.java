package pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.concurrent.Shared;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.exception.ApplicationException;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileMapper;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileDeleteMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileDownloadMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.nio.file.StandardWatchEventKinds.*;
import static pl.dmcs.ujazdowski.filesynchronizer.core.file.FileConfiguration.FILE_PART_SUFFIX;

public class CatalogWatcher extends Thread {

    private static final Logger log = LoggerFactory.getLogger(CatalogWatcher.class.getSimpleName());

    private final Service service;

    private final WatchService watcher;

    private final Shared<ObservableList<CatalogWatchConnection>> connections = new Shared<>(new ObservableListWrapper<>(new ArrayList<>()));

    private final Collection<Path> discardChanges = Collections.synchronizedSet(new HashSet<>());

    public CatalogWatcher(Service service) throws IOException {
        super(CatalogWatcher.class.getSimpleName());
        this.service = service;
        this.watcher = FileSystems.getDefault().newWatchService();
    }

    @Override
    public void run() {
        log.info("Start catalog watcher");

        while (!Thread.currentThread().isInterrupted()) {
            Optional<WatchKey> event = Optional.ofNullable(this.watcher.poll());
            event.ifPresent(this::processEvents);
        }

        connections.write(connectionsList -> connectionsList.forEach(connection -> connection.watchKey().cancel()));

        try {
            this.watcher.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("Stop catalog watcher");
    }

    private void processEvents(WatchKey watchKey) {
        Boolean change = connections.read(list ->
                list.stream()
                        .filter(item -> item.watchKey().equals(watchKey))
                        .findFirst()
                        .map(item -> {
                            AtomicBoolean result = new AtomicBoolean(false);
                            watchKey.pollEvents().forEach(event -> {
                                if ( this.processEvent(event, item) ) {
                                    result.set(true);
                                }
                            });
                            return result.get();
                        }).orElse(false)
        );
        if ( change ) {
            sortConnections();
        }
        watchKey.reset();
    }

    private boolean processEvent(WatchEvent<?> watchEvent, CatalogWatchConnection catalogWatchConnection) {
        if ( watchEvent.context() == null ) {
            return false;
        }

        Path clientPath = (Path) catalogWatchConnection.watchKey().watchable();
        Path path = Paths.get(clientPath.toString(), watchEvent.context().toString());
        String filename = path.getFileName().toString();
        String name = watchEvent.kind().name();
        AbstractConnection connection = catalogWatchConnection.connection();
        log.debug("File event: {}, {}", name, filename);

        if ( shouldBeDiscarded(catalogWatchConnection, path) ) {
            log.debug("Discarded file: {}", path);
            return false;
        }

        if ( ENTRY_MODIFY.name().equals(name) || ENTRY_CREATE.name().equals(name) ) {
            FileDownloadMessage fileDownloadMessage = new FileDownloadMessage(filename, 0);
            fileDownloadMessage.setSender(connection);
            catalogWatchConnection.newEntry(FileMapper.map(path.toFile()));
            this.service.sendFile(fileDownloadMessage);
        } else if ( ENTRY_DELETE.name().equals(name) ) {
            if ( catalogWatchConnection.removeEntry(filename) ) {
                connection.sendMessage(new FileDeleteMessage(filename));
            }
        } else {
            log.warn("Unexpected value: {}", watchEvent.kind());
        }
        return true;
    }

    public void register(AbstractConnection connection) {
        try {
            Path catalogPath = Paths.get(connection.client().getCatalogPath());
            WatchKey watchKey = catalogPath.register(watcher, ENTRY_DELETE, ENTRY_MODIFY, ENTRY_CREATE);
            connections.write(list -> list.add(
                    new CatalogWatchConnection(
                            watchKey,
                            connection,
                            service.catalogList(connection.client())
                    )
            ));
            sortConnections();
            log.info("Registered catalog {}", catalogPath);
        } catch (Exception e) {
            throw new ApplicationException("Cannot register client: " + connection.client().getName(), e);
        }
    }

    private boolean shouldBeDiscarded(CatalogWatchConnection catalogWatchConnection, Path path) {
        return path.toString().endsWith(FILE_PART_SUFFIX) ||
                this.discardChanges.contains(path) ||
                !catalogWatchConnection.hasChanges(path);
    }

    public void receivedNewFile(FileDescription fileDescription, AbstractConnection connection) {
        connections.write(list -> list.stream()
                .filter(catalogWatchConnection -> catalogWatchConnection.connection().client().equals(connection.client()))
                .findFirst()
                .ifPresent(catalogWatchConnection -> catalogWatchConnection.newEntry(fileDescription)));
        sortConnections();
    }

    public void removeFile(FileDeleteMessage message) {
        connections.write(list -> list.stream()
                .filter(catalogWatchConnection -> catalogWatchConnection.connection().client().equals(message.sender().client()))
                .findFirst()
                .ifPresent(catalogWatchConnection -> catalogWatchConnection.removeEntry(message.filename())));
        sortConnections();
    }

    public Shared<ObservableList<CatalogWatchConnection>> connections() {
        return connections;
    }

    private void sortConnections() {
        connections.write(list -> list.sort(Comparator.comparing(o -> o.connection().client().getName())));
    }

    public void unregister(AbstractConnection connection) {
        connections.write(
                list -> list.stream()
                        .filter(item -> item.connection().client().equals(connection.client()))
                        .findFirst().ifPresent(item -> {
                            item.watchKey().cancel();
                            list.remove(item);
                        }));
        sortConnections();
    }

}
