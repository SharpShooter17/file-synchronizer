package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileDeleteMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

public class FileDeleteMessageHandler extends MessageHandler<FileDeleteMessage, AbstractConnection> {

    private final Service service;

    public FileDeleteMessageHandler(Service service) {
        super(MessageType.FILE_DELETE);
        this.service = service;
    }

    @Override
    protected void handle(FileDeleteMessage message) {
        log.info("Handle file delete {}", message);
        service.deleteFile(message);
    }

}
