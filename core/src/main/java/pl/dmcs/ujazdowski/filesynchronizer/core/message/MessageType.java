package pl.dmcs.ujazdowski.filesynchronizer.core.message;

public enum MessageType {

    REGISTER,
    UNREGISTER,
    UNHANDLED,
    FILE_LIST,
    FILE_LIST_RESPONSE,
    FILE_DOWNLOAD,
    FILE_DELETE,
    FILE_PART

}
