package pl.dmcs.ujazdowski.filesynchronizer.core.concurrent;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;

public class Shared<E> {

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private final E list;

    public Shared(E list) {
        this.list = list;
    }

    private void readLock() {
        lock.readLock().lock();
    }

    public <T> T read(Function<E, T> readAction) {
        readLock();
        T result = readAction.apply(list);
        readUnlock();
        return result;
    }

    public void write(Consumer<E> readAction) {
        writeWithResult(collection -> {
            readAction.accept(collection);
            return null;
        });
    }

    public <T> T writeWithResult(Function<E, T> readAction) {
        writeLock();
        T result = readAction.apply(list);
        writeUnlock();
        return result;
    }

    private void readUnlock() {
        lock.readLock().unlock();
    }

    private void writeLock() {
        lock.writeLock().lock();
    }

    private void writeUnlock() {
        lock.writeLock().unlock();
    }

}
