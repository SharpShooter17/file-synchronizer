package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

public class FileListResponse extends Message implements Serializable {

    private static final long serialVersionUID = 202460983392612485L;

    private final Collection<FileDescription> files;

    public FileListResponse(Collection<FileDescription> files) {
        super(MessageType.FILE_LIST_RESPONSE);
        this.files = files;
    }

    public Collection<FileDescription> files() {
        return files;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        if ( !super.equals(o) ) return false;
        FileListResponse that = (FileListResponse) o;
        return Objects.equals(files, that.files);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), files);
    }

    @Override
    public String toString() {
        return "FileListResponse{" +
                "files=" + files +
                '}';
    }

}
