package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import java.io.Serializable;
import java.util.Objects;

public class FileDownloadMessage extends Message implements Serializable {

    private static final long serialVersionUID = 8696811664554351406L;

    private final String filename;

    private final long offset;

    public FileDownloadMessage(String filename, long offset) {
        super(MessageType.FILE_DOWNLOAD);
        this.filename = filename;
        this.offset = offset;
    }

    public String filename() {
        return filename;
    }

    public long offset() {
        return offset;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        if ( !super.equals(o) ) return false;
        FileDownloadMessage that = (FileDownloadMessage) o;
        return offset == that.offset &&
                Objects.equals(filename, that.filename);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), filename, offset);
    }

    @Override
    public String toString() {
        return "FileDownloadMessage{" +
                "filename='" + filename + '\'' +
                ", offset=" + offset +
                "} " + super.toString();
    }

}
