package pl.dmcs.ujazdowski.filesynchronizer.core.task;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.Message;

import java.util.UUID;

public class Task<T extends ConnectionI> {

    private final String uuid = UUID.randomUUID().toString();

    private final T connection;

    private final Message message;

    public Task(T connection, Message message) {
        this.connection = connection;
        this.message = message;
    }

    public T client() {
        return connection;
    }

    public Message message() {
        return message;
    }

    @Override
    public String toString() {
        return "Task{" +
                "uuid='" + uuid + '\'' +
                ", connectedClient=" + connection +
                ", message=" + message +
                '}';
    }

}
