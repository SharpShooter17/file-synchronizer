package pl.dmcs.ujazdowski.filesynchronizer.core.file;

import java.io.File;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class FileMapper {

    public static Collection<FileDescription> mapAll(File[] files) {
        return Arrays.stream(Optional.ofNullable(files).orElse(new File[]{}))
                .map(FileMapper::map)
                .collect(Collectors.toList());
    }

    public static FileDescription map(File file) {
        return new FileDescription(file.getName(), file.length(), Instant.ofEpochMilli(file.lastModified()));
    }

    private FileMapper() {
    }

}
