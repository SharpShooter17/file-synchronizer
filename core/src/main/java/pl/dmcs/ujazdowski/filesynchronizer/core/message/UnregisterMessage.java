package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import java.io.Serializable;

public class UnregisterMessage extends Message implements Serializable {

    private static final long serialVersionUID = 8453913692152629077L;

    public UnregisterMessage() {
        super(MessageType.UNREGISTER);
    }

    @Override
    public String toString() {
        return "UnregisterMessage{} " + super.toString();
    }

}
