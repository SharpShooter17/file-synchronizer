package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.core.task.Task;
import pl.dmcs.ujazdowski.filesynchronizer.core.task.TaskService;

public class MessageListener<T extends ConnectionI> extends Thread {

    private static final Logger log = LoggerFactory.getLogger(MessageListener.class);

    private final T connection;

    private final TaskService<T> taskService;

    public MessageListener(T connection, TaskService<T> taskService) {
        super(MessageListener.class.getSimpleName());
        this.connection = connection;
        this.taskService = taskService;
    }

    @Override
    public void run() {
        log.info("Message listener start");

        while (!Thread.currentThread().isInterrupted()) {
            connection.invokeIfHasMessage(this::process);
        }

        log.info("Message listener stop");
    }

    private void process(T client) {
        try {
            Message message = client.readMessage();
            taskService.addTask(
                    new Task<>(client, message)
            );
        } catch (Exception e) {
            log.info("Message process exception: ", e);
        }
    }

}
