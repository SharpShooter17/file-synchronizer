package pl.dmcs.ujazdowski.filesynchronizer.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileMapper;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.receiver.FileReceiverSupervisor;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileSenderSupervisor;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileToSend;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher.CatalogWatcher;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileDeleteMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileDownloadMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FilePartMessage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static pl.dmcs.ujazdowski.filesynchronizer.core.file.FileConfiguration.FILE_PART_SUFFIX;

public abstract class Service {

    private static final Logger log = LoggerFactory.getLogger(Service.class.getSimpleName());

    protected FileReceiverSupervisor fileReceiverSupervisor;

    protected FileSenderSupervisor fileSenderSupervisor;

    protected CatalogWatcher catalogWatcher;

    public void register(AbstractConnection connection, Client client) {
        connection.register(client);
        Path clientCatalogPath = Paths.get(client.getCatalogPath());
        createDirectoryIfNotExists(clientCatalogPath.toFile());
        catalogWatcher.register(connection);
    }

    public void unregister(AbstractConnection connection) {
        connection.disconnect();
        catalogWatcher.unregister(connection);
    }

    public Collection<FileDownloadMessage> filesToUpdate(Client client, Collection<FileDescription> secondSideFiles) {
        File userCatalog = new File(client.getCatalogPath());

        List<FileDescription> currentFiles = Arrays.stream(Optional.ofNullable(userCatalog.listFiles()).orElse(new File[]{}))
                .map(FileMapper::map)
                .collect(Collectors.toList());

        Collection<FileDownloadMessage> shouldBeResumed = resumeDownloadingFiles(client);

        List<FileDownloadMessage> downloadMessages = secondSideFiles.stream()
                .filter(secondSideFile -> this.shouldUpdateFile(secondSideFile, currentFiles))
                .map(file -> {
                    Optional<FileDownloadMessage> resumeDownloading = shouldBeResumed.stream()
                            .filter(resumed -> resumed.filename().equals(file.getName()))
                            .findFirst();
                    resumeDownloading.ifPresent(shouldBeResumed::remove);
                    long offset = resumeDownloading.map(FileDownloadMessage::offset).orElse(0L);
                    return new FileDownloadMessage(file.getName(), offset);
                })
                .collect(Collectors.toList());

        shouldBeResumed.forEach(filepart -> deleteFile(filePath(client, filepart.filename() + FILE_PART_SUFFIX)));

        return downloadMessages;
    }

    private boolean shouldUpdateFile(FileDescription secondSideFile, List<FileDescription> currentFiles) {
        FileDescription currentFile = currentFiles.stream()
                .filter(userFile -> userFile.getName().equals(secondSideFile.getName()))
                .findFirst()
                .orElse(null);

        if ( currentFile != null ) {
            return currentFile.getLastTimeModification().isBefore(secondSideFile.getLastTimeModification());
        } else {
            return true;
        }
    }

    public Collection<FileDownloadMessage> resumeDownloadingFiles(Client client) {
        File userCatalog = new File(client.getCatalogPath());
        return Arrays.stream(Optional.ofNullable(userCatalog.listFiles()).orElse(new File[]{}))
                .map(FileMapper::map)
                .filter(file -> file.getName().endsWith(FILE_PART_SUFFIX))
                .map(file -> new FileDownloadMessage(file.getName().replaceAll(FILE_PART_SUFFIX + '$', ""), file.getSize()))
                .collect(Collectors.toList());
    }

    public void saveFile(FilePartMessage filePartMessage) {
        fileReceiverSupervisor.addFilePart(filePartMessage);
    }

    public Collection<FileDescription> catalogList(Client client) {
        File userDirectory = new File(client.getCatalogPath());
        createDirectoryIfNotExists(userDirectory);
        return FileMapper.mapAll(userDirectory.listFiles())
                .stream()
                .filter(file -> !file.getName().endsWith(FILE_PART_SUFFIX))
                .collect(Collectors.toList());
    }

    private void createDirectoryIfNotExists(File directory) {
        if ( !directory.exists() ) {
            directory.mkdir();
        }
    }

    public void sendFile(FileDownloadMessage message) {
        AbstractConnection connection = message.sender();
        File file = filePath(connection.client(), message.filename()).toFile();
        FileToSend fileToSend = new FileToSend(connection.client().getPriority().value, connection, file, message.offset());
        fileSenderSupervisor.queueFileToSend(fileToSend);
    }

    public void deleteFile(FileDeleteMessage message) {
        Path filePath = filePath(message.sender().client(), message.filename());
        catalogWatcher.removeFile(message);
        deleteFile(filePath);
    }

    private Path filePath(Client client, String filename) {
        return Paths.get(client.getCatalogPath(), filename);
    }

    public void setFileReceiverSupervisor(FileReceiverSupervisor fileReceiverSupervisor) {
        if ( this.fileReceiverSupervisor != null ) {
            throw new IllegalStateException();
        }
        this.fileReceiverSupervisor = fileReceiverSupervisor;
    }

    public void setFileSenderSupervisor(FileSenderSupervisor fileSenderSupervisor) {
        if ( this.fileSenderSupervisor != null ) {
            throw new IllegalStateException();
        }
        this.fileSenderSupervisor = fileSenderSupervisor;
    }

    public void setCatalogWatcher(CatalogWatcher catalogWatcher) {
        this.catalogWatcher = catalogWatcher;
    }

    private void deleteFile(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            log.error("Cannot delete file: {}", path, e);
        }
    }

}
