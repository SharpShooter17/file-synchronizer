package pl.dmcs.ujazdowski.filesynchronizer.core.runner;

import pl.dmcs.ujazdowski.filesynchronizer.core.api.FileSynchronizerApi;
import pl.dmcs.ujazdowski.filesynchronizer.core.config.Configuration;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.receiver.FileReceiverSupervisor;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileSenderSupervisor;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher.CatalogWatcher;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageListener;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.UnregisterMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;
import pl.dmcs.ujazdowski.filesynchronizer.core.task.TaskProcessor;
import pl.dmcs.ujazdowski.filesynchronizer.core.task.TaskService;

import java.io.IOException;

public class FileSynchronizerRunner<C extends ConnectionI, S extends Service> {

    private final FileReceiverSupervisor fileReceiverSupervisor;

    private final FileSenderSupervisor fileSenderSupervisor;

    private final TaskProcessor taskProcessor;

    private final MessageListener<C> messageListener;

    private final CatalogWatcher catalogWatcher;

    private final FileSynchronizerApi<C> fileSynchronizerApi;

    protected final C connection;

    public FileSynchronizerRunner(Configuration<C, S> configuration) throws IOException {
        this.connection = configuration.connection;
        this.catalogWatcher = new CatalogWatcher(configuration.service);
        this.fileReceiverSupervisor = new FileReceiverSupervisor(this.catalogWatcher);
        this.fileSenderSupervisor = new FileSenderSupervisor(configuration.maximumFileSenderThreads);
        configuration.service.setFileReceiverSupervisor(this.fileReceiverSupervisor);
        configuration.service.setFileSenderSupervisor(this.fileSenderSupervisor);
        configuration.service.setCatalogWatcher(this.catalogWatcher);
        TaskService<C> taskService = new TaskService<>();
        this.messageListener = new MessageListener<>(this.connection, taskService);
        this.taskProcessor = new TaskProcessor(taskService, configuration.messageHandler);
        this.fileSynchronizerApi =
                new FileSynchronizerApi<>(
                        this.catalogWatcher.connections(),
                        this.fileSenderSupervisor.filesToSend(),
                        this.fileSenderSupervisor.fileSenders(),
                        this.fileReceiverSupervisor.runningReceivers(),
                        this
                );
    }

    public void start() {
        fileReceiverSupervisor.start();
        fileSenderSupervisor.start();
        taskProcessor.start();
        messageListener.start();
        catalogWatcher.start();
    }

    public void join() throws InterruptedException {
        fileReceiverSupervisor.join();
        fileSenderSupervisor.join();
        taskProcessor.join();
        messageListener.join();
        catalogWatcher.join();
    }

    public void stop() {
        this.connection.sendMessage(new UnregisterMessage());
        this.stopThreads();
    }

    public void stopThreads() {
        this.catalogWatcher.interrupt();
        this.messageListener.interrupt();
        this.taskProcessor.interrupt();
        this.fileSenderSupervisor.interrupt();
        this.fileReceiverSupervisor.interrupt();
    }

    public FileSynchronizerApi<C> api() {
        return fileSynchronizerApi;
    }

}
