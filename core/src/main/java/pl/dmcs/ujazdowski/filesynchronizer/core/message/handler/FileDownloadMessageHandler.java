package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileDownloadMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

public class FileDownloadMessageHandler extends MessageHandler<FileDownloadMessage, AbstractConnection> {

    private final Service service;

    public FileDownloadMessageHandler(Service service) {
        super(MessageType.FILE_DOWNLOAD);
        this.service = service;
    }

    @Override
    protected void handle(FileDownloadMessage message) {
        log.info("Handle file download {}", message);
        service.sendFile(message);
    }

}
