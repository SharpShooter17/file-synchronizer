package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import java.io.Serializable;

public class FileListMessage extends Message implements Serializable {

    private static final long serialVersionUID = 4875082337992884419L;

    public FileListMessage() {
        super(MessageType.FILE_LIST);
    }

    @Override
    public String toString() {
        return "FileListMessage{}";
    }

}
