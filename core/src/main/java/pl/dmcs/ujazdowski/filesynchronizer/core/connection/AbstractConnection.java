package pl.dmcs.ujazdowski.filesynchronizer.core.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;
import pl.dmcs.ujazdowski.filesynchronizer.core.exception.ApplicationException;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.Message;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.UnhandledMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.function.Consumer;

public class AbstractConnection implements ConnectionI {

    protected final Logger log;

    protected Client client;

    protected final Socket socket;

    protected final ObjectOutputStream outputStream;

    protected final ObjectInputStream inputStream;

    public AbstractConnection(Socket socket) throws IOException {
        this.socket = socket;
        this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        this.outputStream.flush();
        this.inputStream = new ObjectInputStream(socket.getInputStream());

        if ( !socket.isConnected() ) {
            throw new ApplicationException("Cannot connect!");
        }

        this.log = LoggerFactory.getLogger("ConnectedClient: " + socket.getInetAddress() + ":" + socket.getPort());
    }

    public boolean isRegistered() {
        return this.client != null;
    }

    public void register(Client client) {
        if ( this.isRegistered() ) {
            throw new ApplicationException("Client is currently registered");
        }
        this.client = client;
        log.info("Registered {}", client);
    }

    @Override
    public synchronized Message readMessage() {
        try {
            this.inputStream.readInt();
            return (Message) this.inputStream.readObject();
        } catch (Exception e) {
            log.error("Cannot read message", e);
            return new UnhandledMessage();
        }
    }

    @Override
    public <T extends ConnectionI> void invokeIfHasMessage(Consumer<T> consumer) {
        if ( this.hasMessage() ) {
            consumer.accept((T) this);
        }
    }

    public synchronized void sendMessage(Message message) {
        try {
            log.debug("Sending message: {}", message);
            outputStream.writeInt(1);
            outputStream.writeObject(message);
            outputStream.flush();
        } catch (IOException e) {
            throw new ApplicationException("Cannot sent message", e);
        }
    }

    public boolean hasMessage() {
        try {
            return inputStream.available() > 0;
        } catch (IOException e) {
            throw new ApplicationException("Has message failed", e);
        }
    }

    public void disconnect() {
        try {
            log.info("Disconnecting...");
            this.inputStream.close();
            this.outputStream.close();
            this.socket.close();
            log.info("Disconnected");
        } catch (IOException e) {
            log.error("Cannot disconnect", e);
        }
    }

    public Socket socket() {
        return this.socket;
    }

    public Client client() {
        return this.client;
    }

    @Override
    public String toString() {
        return socket.getInetAddress() + ":" + socket.getPort();
    }

}
