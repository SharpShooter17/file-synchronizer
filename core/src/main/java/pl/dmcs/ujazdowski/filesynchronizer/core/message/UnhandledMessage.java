package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import java.io.Serializable;

public class UnhandledMessage extends Message implements Serializable {

    private static final long serialVersionUID = 3437835883584712764L;

    public UnhandledMessage() {
        super(MessageType.UNHANDLED);
    }

    @Override
    public String toString() {
        return "UnhandledMessage{} " + super.toString();
    }

}
