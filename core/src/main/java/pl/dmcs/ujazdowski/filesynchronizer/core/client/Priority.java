package pl.dmcs.ujazdowski.filesynchronizer.core.client;

import java.io.Serializable;

public enum Priority implements Serializable {

    LOW(0),
    HIGH(30);

    public final int value;

    Priority(int value) {
        this.value = value;
    }
}
