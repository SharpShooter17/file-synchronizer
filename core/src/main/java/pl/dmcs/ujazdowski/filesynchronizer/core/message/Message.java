package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public abstract class Message implements Serializable {

    private static final long serialVersionUID = -6515000490996252020L;

    private transient AbstractConnection sender;

    private final Instant messageTime = Instant.now();

    private final MessageType type;

    protected Message(MessageType type) {
        this.type = type;
    }

    public MessageType type() {
        return this.type;
    }

    public AbstractConnection sender() {
        return sender;
    }

    public Message setSender(AbstractConnection sender) {
        this.sender = sender;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        Message message = (Message) o;
        return Objects.equals(messageTime, message.messageTime) &&
                type == message.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageTime, type);
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageTime=" + messageTime +
                ", type=" + type +
                '}';
    }

}
