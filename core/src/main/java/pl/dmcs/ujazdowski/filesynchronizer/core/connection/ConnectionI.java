package pl.dmcs.ujazdowski.filesynchronizer.core.connection;

import pl.dmcs.ujazdowski.filesynchronizer.core.message.Message;

import java.util.function.Consumer;

public interface ConnectionI {

    Message readMessage();

    <T extends ConnectionI> void invokeIfHasMessage(Consumer<T> consumer);

    void sendMessage(Message message);

}
