package pl.dmcs.ujazdowski.filesynchronizer.core.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TaskService<C extends ConnectionI> {

    private static final Logger log = LoggerFactory.getLogger(TaskService.class);

    private final Queue<Task<C>> queue = new ConcurrentLinkedQueue<>();

    public void addTask(Task<C> task) {
        log.debug("Add task {}", task);
        queue.add(task);
    }

    public Task<C> nextTask() {
        return queue.poll();
    }

}
