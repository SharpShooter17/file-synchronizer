package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import java.io.Serializable;
import java.util.Objects;

public class FileDeleteMessage extends Message implements Serializable {

    private static final long serialVersionUID = 305391755210369774L;

    private final String filename;

    public FileDeleteMessage(String filename) {
        super(MessageType.FILE_DELETE);
        this.filename = filename;
    }

    public String filename() {
        return filename;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        if ( !super.equals(o) ) return false;
        FileDeleteMessage that = (FileDeleteMessage) o;
        return Objects.equals(filename, that.filename);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), filename);
    }

    @Override
    public String toString() {
        return "FileDeleteMessage{" +
                "filename='" + filename + '\'' +
                '}';
    }

}
