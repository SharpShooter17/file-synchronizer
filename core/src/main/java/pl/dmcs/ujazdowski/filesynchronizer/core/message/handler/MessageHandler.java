package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.Message;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;

import java.util.Optional;

public abstract class MessageHandler<M extends Message, C extends AbstractConnection> {

    protected static final Logger log = LoggerFactory.getLogger(MessageHandler.class);

    private Optional<MessageHandler> nextHandler;

    private final MessageType messageTypeToHandle;

    protected MessageHandler(MessageType messageTypeToHandle) {
        this.messageTypeToHandle = messageTypeToHandle;
        this.nextHandler = Optional.empty();
    }

    public void apply(C connection, Message message) {
        if ( messageTypeToHandle == message.type() ) {
            message.setSender(connection);
            this.handle((M) message);
        } else {
            nextHandler.ifPresent(handler -> handler.apply(connection, message));
        }
    }

    protected abstract void handle(M message);

    public MessageHandler next(MessageHandler handler) {
        this.nextHandler = Optional.of(handler);
        return handler;
    }

}
