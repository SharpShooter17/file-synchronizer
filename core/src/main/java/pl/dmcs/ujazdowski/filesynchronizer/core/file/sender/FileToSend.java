package pl.dmcs.ujazdowski.filesynchronizer.core.file.sender;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class FileToSend implements Comparable<FileToSend> {

    private final BigDecimal priority;

    private final AbstractConnection connection;

    private final File file;

    private long offset;

    public FileToSend(int initialPriority,
                      AbstractConnection connection,
                      File file,
                      long offset) {
        BigDecimal fileLength = BigDecimal.valueOf(file.length(), 20);
        this.priority = BigDecimal.ONE
                .divide(fileLength.signum() == 0 ? BigDecimal.valueOf(0.0001) : fileLength, RoundingMode.HALF_UP)
                .multiply(BigDecimal.valueOf(1000, 20))
                .add(BigDecimal.valueOf(initialPriority, 20));
        this.connection = connection;
        this.file = file;
        this.offset = offset;
    }

    public BigDecimal priority() {
        return priority;
    }

    public AbstractConnection connection() {
        return connection;
    }

    public long offset() {
        return offset;
    }

    public void moveOffset(int bytes) {
        this.offset += bytes;
    }

    public File file() {
        return file;
    }

    @Override
    public int compareTo(FileToSend o) {
        return this.priority().compareTo(o.priority());
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        FileToSend that = (FileToSend) o;
        return priority.equals(that.priority) &&
                Objects.equals(connection, that.connection) &&
                Objects.equals(file, that.file);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priority, connection, file);
    }

    @Override
    public String toString() {
        return "FileToSend{" +
                "getPriority=" + priority() +
                ", sender=" + connection +
                ", file=" + file +
                ", offset=" + offset +
                '}';
    }

}
