package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class FilePartMessage extends Message implements Serializable {

    private static final long serialVersionUID = 2160169201348262765L;

    private String filename;

    private final long size;

    private final long modificationTimestamp;

    private final long offset;

    private final byte[] filePart;

    public FilePartMessage(String filename,
                           long size, long modificationTimestamp, long offset,
                           byte[] filePart) {
        super(MessageType.FILE_PART);
        this.filename = filename;
        this.size = size;
        this.modificationTimestamp = modificationTimestamp;
        this.offset = offset;
        this.filePart = filePart;
    }

    public String filename() {
        return filename;
    }

    public void filename(String filename) {
        this.filename = filename;
    }

    public long size() {
        return size;
    }

    public long offset() {
        return offset;
    }

    public byte[] filePart() {
        return filePart;
    }

    public long modificationTimestamp() {
        return this.modificationTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        if ( !super.equals(o) ) return false;
        FilePartMessage that = (FilePartMessage) o;
        return size == that.size &&
                modificationTimestamp == that.modificationTimestamp &&
                offset == that.offset &&
                Objects.equals(filename, that.filename) &&
                Arrays.equals(filePart, that.filePart);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), filename, size, modificationTimestamp, offset);
        result = 31 * result + Arrays.hashCode(filePart);
        return result;
    }

    @Override
    public String toString() {
        return "FilePartMessage{" +
                "filename='" + filename + '\'' +
                ", offset=" + offset +
                '}';
    }

}
