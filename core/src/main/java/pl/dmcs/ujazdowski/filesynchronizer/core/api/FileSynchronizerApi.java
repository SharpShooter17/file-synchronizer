package pl.dmcs.ujazdowski.filesynchronizer.core.api;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import pl.dmcs.ujazdowski.filesynchronizer.core.concurrent.Shared;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.receiver.FileReceiver;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileSender;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.sender.FileToSend;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher.CatalogWatchConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.runner.FileSynchronizerRunner;

import java.util.Collection;
import java.util.stream.Collectors;

public class FileSynchronizerApi<C extends ConnectionI> {

    private String name;

    private final Shared<ObservableList<CatalogWatchConnection>> connections;

    private final Shared<ObservableList<FileToSend>> fileToSends;

    private final Shared<ObservableList<FileSender>> fileSenders;

    private final Shared<ObservableList<FileReceiver>> receivers;

    private final FileSynchronizerRunner runner;

    public FileSynchronizerApi(Shared<ObservableList<CatalogWatchConnection>> connections,
                               Shared<ObservableList<FileToSend>> fileToSends,
                               Shared<ObservableList<FileSender>> fileSenders,
                               Shared<ObservableList<FileReceiver>> receivers,
                               FileSynchronizerRunner runner) {
        this.connections = connections;
        this.fileToSends = fileToSends;
        this.fileSenders = fileSenders;
        this.receivers = receivers;
        this.runner = runner;
    }

    public String getName() {
        return name;
    }

    public FileSynchronizerApi<C> setName(String name) {
        this.name = name;
        return this;
    }

    public void addConnectionListener(ListChangeListener<CatalogWatchConnection> listener) {
        connections.write(list -> list.addListener(listener));
    }

    public Shared<ObservableList<CatalogWatchConnection>> getConnections() {
        return this.connections;
    }

    public void addFilesToSendListener(ListChangeListener<FileToSend> listener) {
        fileToSends.write(list -> list.addListener(listener));
    }

    public Shared<ObservableList<FileToSend>> filesToSend() {
        return this.fileToSends;
    }

    public void addFileSendersListener(ListChangeListener<FileSender> listener) {
        fileSenders.write(list -> list.addListener(listener));
    }

    public Collection<FileToSend> sendingFiles() {
        return this.fileSenders.read(list -> list.stream().map(FileSender::fileToSend).collect(Collectors.toList()));
    }


    public void addReceiversListener(ListChangeListener<FileReceiver> listener) {
        receivers.write(list -> list.addListener(listener));
    }

    public Shared<ObservableList<FileReceiver>> fileReceivers() {
        return this.receivers;
    }

    public void shutdown() {
        runner.stop();
    }

}
