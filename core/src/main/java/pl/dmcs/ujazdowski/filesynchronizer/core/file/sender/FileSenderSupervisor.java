package pl.dmcs.ujazdowski.filesynchronizer.core.file.sender;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.concurrent.Shared;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FileSenderSupervisor extends Thread {

    private static final Logger log = LoggerFactory.getLogger(FileSenderSupervisor.class);

    private final Shared<ObservableList<FileToSend>> filesToSend = new Shared<>(new ObservableListWrapper<>(new ArrayList<>()));

    private final int maximumFileSenderThreads;

    private final Shared<ObservableList<FileSender>> fileSenders = new Shared<>(new ObservableListWrapper<>(new ArrayList<>()));

    public FileSenderSupervisor(int maximumFileSenderThreads) {
        super(FileSenderSupervisor.class.getSimpleName());
        this.maximumFileSenderThreads = maximumFileSenderThreads;
    }

    @Override
    public void run() {
        log.info("Start File Sender Supervisor");

        while (!Thread.currentThread().isInterrupted()) {
            clearSenders();

            if ( filesToSend.read(List::isEmpty) ) {
                Thread.yield();
                continue;
            }

            FileToSend file = filesToSend.read(list -> list.get(0));

            if ( fileSenders.read(List::size) < maximumFileSenderThreads ||
                    removeSenderWithLowerPriority(file) ) {
                startSender(file);
            }
        }

        fileSenders.write(list -> list.forEach(Thread::interrupt));

        log.info("Stop File Sender Supervisor");
    }

    private boolean removeSenderWithLowerPriority(FileToSend file) {
        Optional<FileSender> fileSenderWithLowerPriority = this.fileSenders.read(list ->
                list.stream().filter(sender -> sender.fileToSend().priority().compareTo(file.priority()) < 0)
                        .findFirst());

        fileSenderWithLowerPriority.ifPresent(sender -> {
            if ( this.fileSenders.writeWithResult(list -> list.remove(sender)) ) {
                sender.interrupt();
                this.queueFileToSend(sender.fileToSend());
                try {
                    sender.join(10000);
                } catch (InterruptedException e) {
                    log.error("Sender cannot join {}", sender, e);
                }
                log.info("Remove sender: {} with lower getPriority for file: {}", sender.fileToSend().file().getName(), file.file().getName());
            } else {
                log.error("Cannot remove sender from collection {}", sender.fileToSend());
            }
        });

        return fileSenderWithLowerPriority.isPresent();
    }

    private void startSender(FileToSend fileToSend) {
        FileSender sender = new FileSender(fileToSend);
        fileSenders.write(list -> list.add(sender));
        sender.start();
        filesToSend.write(list -> list.remove(fileToSend));
    }

    private void clearSenders() {
        fileSenders.write(list -> list.removeIf(sender -> !sender.isAlive()));
    }

    public void queueFileToSend(FileToSend fileToSend) {
        if ( !this.filesToSend.writeWithResult(list -> list.add(fileToSend)) ) {
            log.warn("Cannot add file to send list: {}", fileToSend);
        } else {
            this.filesToSend.write(list -> list.sort((o1, o2) -> -1 * o1.compareTo(o2)));
        }
    }

    public final Shared<ObservableList<FileToSend>> filesToSend() {
        return this.filesToSend;
    }

    public final Shared<ObservableList<FileSender>> fileSenders() {
        return this.fileSenders;
    }

}