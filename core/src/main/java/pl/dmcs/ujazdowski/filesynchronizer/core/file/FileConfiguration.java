package pl.dmcs.ujazdowski.filesynchronizer.core.file;

public class FileConfiguration {

    public static final String FILE_PART_SUFFIX = ".filepart";

    public static final long FILE_RECEIVING_TIMEOUT_MS = 1000;

    private FileConfiguration() {
    }

}
