package pl.dmcs.ujazdowski.filesynchronizer.core.file.watcher;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.WatchKey;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class CatalogWatchConnection {

    private final WatchKey watchKey;

    private final AbstractConnection connection;

    private final Collection<FileDescription> fileList = Collections.synchronizedSet(new HashSet<>());

    public CatalogWatchConnection(WatchKey watchKey,
                                  AbstractConnection connection,
                                  Collection<FileDescription> fileList) {
        this.watchKey = watchKey;
        this.connection = connection;
        this.fileList.addAll(fileList);
    }

    public WatchKey watchKey() {
        return watchKey;
    }

    public AbstractConnection connection() {
        return connection;
    }

    public boolean hasChanges(Path path) {
        return this.fileList.stream()
                .filter(file -> file.getName().equals(path.getFileName().toString()))
                .findFirst()
                .map(file -> {
                    File currentFile = path.toFile();
                    return !currentFile.exists() || file.getLastTimeModification().toEpochMilli() < currentFile.lastModified();
                })
                .orElse(true);
    }

    public void newEntry(FileDescription file) {
        this.fileList.remove(file);
        this.fileList.add(file);
    }

    public boolean removeEntry(String filename) {
        return this.fileList.removeIf(file -> file.getName().equals(filename));
    }

    public final Collection<FileDescription> files() {
        return this.fileList;
    }

}
