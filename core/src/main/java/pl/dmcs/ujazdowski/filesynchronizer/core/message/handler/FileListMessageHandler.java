package pl.dmcs.ujazdowski.filesynchronizer.core.message.handler;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.file.FileDescription;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileListMessage;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FileListResponse;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.MessageType;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

import java.util.Collection;

public class FileListMessageHandler extends MessageHandler<FileListMessage, AbstractConnection> {

    private final Service service;

    public FileListMessageHandler(Service service) {
        super(MessageType.FILE_LIST);
        this.service = service;
    }

    @Override
    protected void handle(FileListMessage message) {
        Collection<FileDescription> files = service.catalogList(message.sender().client());
        message.sender().sendMessage(new FileListResponse(files));
    }

}
