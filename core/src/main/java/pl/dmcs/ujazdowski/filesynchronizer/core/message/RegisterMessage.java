package pl.dmcs.ujazdowski.filesynchronizer.core.message;

import pl.dmcs.ujazdowski.filesynchronizer.core.client.Client;

import java.io.Serializable;
import java.util.Objects;

public class RegisterMessage extends Message implements Serializable {

    private static final long serialVersionUID = 1700523944566110360L;

    private final Client client;

    public RegisterMessage(Client client) {
        super(MessageType.REGISTER);
        this.client = client;
    }

    public Client client() {
        return this.client;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        if ( !super.equals(o) ) return false;
        RegisterMessage that = (RegisterMessage) o;
        return Objects.equals(client, that.client);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), client);
    }

    @Override
    public String toString() {
        return "RegisterMessage{" +
                "client=" + client +
                '}';
    }

}
