package pl.dmcs.ujazdowski.filesynchronizer.core.config;

import pl.dmcs.ujazdowski.filesynchronizer.core.connection.ConnectionI;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.handler.MessageHandler;
import pl.dmcs.ujazdowski.filesynchronizer.core.service.Service;

public class Configuration<C extends ConnectionI, S extends Service> {

    public final int maximumFileSenderThreads;

    public final C connection;

    public final MessageHandler messageHandler;

    public final S service;

    public Configuration(int maximumFileSenderThreads,
                         C connection,
                         MessageHandler messageHandler,
                         S service) {
        this.maximumFileSenderThreads = maximumFileSenderThreads;
        this.connection = connection;
        this.messageHandler = messageHandler;
        this.service = service;
    }

}
