package pl.dmcs.ujazdowski.filesynchronizer.core.file.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.exception.ApplicationException;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.FilePartMessage;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class FileSender extends Thread {

    private final Logger log;

    private final FileToSend fileToSend;

    private static final int MAX_PART_FILE_SIZE = 512;

    public FileSender(FileToSend fileToSend) {
        super(FileSender.class.getSimpleName() + " - " + fileToSend.file().getPath());
        this.fileToSend = fileToSend;
        this.log = LoggerFactory.getLogger(this.getName());
    }

    @Override
    public void run() {
        log.info("Start sending file {}", fileToSend.file().getName());

        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(fileToSend.file()))) {
            long skippedBytes = inputStream.skip(fileToSend.offset());
            if ( skippedBytes != fileToSend.offset() ) {
                log.error("Cannot jump to offset: {}, skipped: {} bytes", fileToSend.offset(), skippedBytes);
            }
            do {
                sendFilePart(inputStream);
            } while (!Thread.currentThread().isInterrupted() && isSending());
        } catch (Exception e) {
            throw new ApplicationException("Cannot open stream to file", e);
        }

        log.info("Stop sending file {}", fileToSend.file().getName());
    }

    private void sendFilePart(InputStream inputStream) {
        final int partSize = (int) Math.min(fileToSend.file().length() - fileToSend.offset(), MAX_PART_FILE_SIZE);
        byte[] part = new byte[partSize];

        try {
            int readBytes = inputStream.read(part, 0, partSize);
            FilePartMessage filePartMessage =
                    new FilePartMessage(
                            fileToSend.file().getName(),
                            fileToSend.file().length(),
                            fileToSend.file().lastModified(),
                            fileToSend.offset(),
                            part
                    );
            fileToSend.connection().sendMessage(filePartMessage);
            fileToSend.moveOffset(readBytes);
        } catch (Exception e) {
            throw new ApplicationException("Cannot read file", e);
        }
    }

    private boolean isSending() {
        return fileToSend.offset() < fileToSend.file().length();
    }

    public final FileToSend fileToSend() {
        return this.fileToSend;
    }

}
