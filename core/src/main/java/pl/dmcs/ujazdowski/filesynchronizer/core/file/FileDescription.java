package pl.dmcs.ujazdowski.filesynchronizer.core.file;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class FileDescription implements Serializable {

    private static final long serialVersionUID = 1559186380726167093L;

    private final String name;

    private final long size;

    private final Instant lastTimeModification;

    public FileDescription(String name, long size, Instant lastTimeModification) {
        this.name = name;
        this.size = size;
        this.lastTimeModification = lastTimeModification;
    }

    public String getName() {
        return name;
    }

    public long getSize() {
        return size;
    }

    public Instant getLastTimeModification() {
        return lastTimeModification;
    }

    @Override
    public String toString() {
        String separator = "\t\t\t\t";
        return getLastTimeModification() + separator +
                getSize() + separator +
                getName();
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        FileDescription that = (FileDescription) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}
