package pl.dmcs.ujazdowski.filesynchronizer.core.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.ujazdowski.filesynchronizer.core.connection.AbstractConnection;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.Message;
import pl.dmcs.ujazdowski.filesynchronizer.core.message.handler.MessageHandler;

import java.util.Optional;

public class TaskProcessor<C extends AbstractConnection> extends Thread {

    private static final Logger log = LoggerFactory.getLogger(TaskProcessor.class);

    private final TaskService<C> service;

    private final MessageHandler<Message, C> messageHandlers;

    public TaskProcessor(TaskService<C> service, MessageHandler<Message, C> messageHandlers) {
        super(TaskProcessor.class.getSimpleName());
        this.service = service;
        this.messageHandlers = messageHandlers;
    }

    @Override
    public void run() {
        log.info("Start Task processor");
        while (!Thread.currentThread().isInterrupted()) {
            Optional<Task<C>> task = Optional.ofNullable(service.nextTask());
            task.ifPresent(this::process);
            Thread.yield();
        }
        log.info("Stop Task processor");
    }

    private void process(Task<C> task) {
        log.debug("Processing task {}", task);
        messageHandlers.apply(task.client(), task.message());
    }

}
