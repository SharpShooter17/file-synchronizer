package pl.dmcs.ujazdowski.filesynchronizer.core.client;

import java.io.Serializable;
import java.util.Objects;

public class Client implements Serializable {

    private static final long serialVersionUID = -8971731985462434193L;

    private final String name;

    private String catalogPath;

    private final Priority priority;

    public Client(String name,
                  String catalogPath,
                  Priority priority) {
        this.name = name;
        this.catalogPath = catalogPath;
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public String getCatalogPath() {
        return catalogPath;
    }

    public void setCatalogPath(String path) {
        this.catalogPath = path;
    }

    public Priority getPriority() {
        return priority;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", setCatalogPath='" + catalogPath + '\'' +
                ", getPriority=" + priority +
                '}';
    }

}
