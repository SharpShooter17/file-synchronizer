SET client.name=Server
SET server.port=11000
SET filesynchronizer.server-port=%1
SET filesynchronizer.server.user.catalog=%2

ECHO ON

call java -jar ./server/target/filesynchronizer-server.jar